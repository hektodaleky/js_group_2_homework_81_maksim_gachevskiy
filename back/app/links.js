const express = require('express');
const router = express.Router();
const nanoid = require('nanoid');
const Link = require('../models/Links');
const ObjectId = require('mongodb').ObjectID;

const createRouter = () => {
    router.get('/link', (req, res) => {
        console.log(req.params.id);
        Link.find().then(response => {
            res.send(response)
        })
            .catch(error=>{
                send(error)
            })
    });

    router.get('/:id', (req, res) => {
        console.log(req.params.id);
        Link.find({shortUrl: req.params.id}).then(response => {
            res.status(301).redirect(response[0].originalUrl)
        })
            .catch(error=>{
                res.sendStatus(400).send(error)
        })
    });

    router.post('/link', (req, res) => {
        const link = req.body;
        link.shortUrl = nanoid(7);
        const linkData = new Link(link);
        linkData.save()
            .then(result => res.send(result))
            .catch(error => res.sendStatus(400).send(error));
    });
    return router;
};
module.exports = createRouter;