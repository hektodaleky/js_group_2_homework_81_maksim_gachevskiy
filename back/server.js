const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const link = require('./app/links');
const app = express();

const port = 8000;
app.use(express.json());

const db = mongoose.connection;

app.use(cors());

mongoose.connect(config.db.url + '/' + config.db.name);
db.once('open', (db) => {
    console.log('Mongoose connected');
    app.use('/', link(db));


    app.listen(port, () => {
        console.log(`Server started on ${port} port`)
    });
});
