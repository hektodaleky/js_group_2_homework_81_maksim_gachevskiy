import axios from "../axios";
export const DATA_REQUEST = 'DATA_REQUEST';
export const DATA_SUCCESS = 'DATA_SUCCESS';
export const DATA_ERROR = 'DATA_ERROR';


export const dataRequest = () => {
    return {type: DATA_REQUEST}
};
export const dataSuccess = (data) => {
    return {type: DATA_SUCCESS, data}
};
export const dataError = () => {
    return {type: DATA_ERROR}
};

export const postData = (postData) => {
    console.log(postData)
    return dispatch => {

        dispatch(dataRequest());
        axios.post(`/link`, postData).then(response => {
            console.log(response)
            dispatch(dataSuccess(response.data));
        }, error => {
            dispatch(dataError())
        });

    }

};

