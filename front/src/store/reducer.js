import {DATA_ERROR, DATA_REQUEST, DATA_SUCCESS} from "./action";
const initialState = {
        link: "",
        loading: false,
        error: false


    }
;
const reducer = (state = initialState, action) => {

    switch (action.type) {
        case DATA_REQUEST: {
            return {...state, loading: true, error: false};
        }
        case DATA_SUCCESS: {
            return {
                ...state,
                loading: false,
                error: false,
                link: 'http://localhost:8000/'+action.data.shortUrl
            }
        }

        case DATA_ERROR: {
            return {...state, loading: false, error: true};
        }


        default:
            return state;
    }


};
export default reducer;