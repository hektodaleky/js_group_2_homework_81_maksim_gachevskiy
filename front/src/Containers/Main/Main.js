import React, {Component} from "react";
import {connect} from "react-redux";

import "./Main.css";
import {postData} from "../../store/action";

class Main extends Component {
    state = {
        text: ""
    };

    changeText = event => {
        this.setState({text: event.target.value})
    };

    render() {
        return (
            <div className="Main">
                <h2>Shorten your link!</h2>
                <input value={this.state.text} onChange={this.changeText}/>
                <button onClick={() => this.props.postData({originalUrl: this.state.text})}>Shorten!</button>
                <h5>Your link now looks like this: </h5>
                <a href={this.props.link}>{this.props.link}</a>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        link: state.link
    }
};

const mapDispatchToProps = dispatch => {
    return {
        postData: (data) => dispatch(postData(data))
    }
        ;
};
export default connect(mapStateToProps, mapDispatchToProps)(Main);